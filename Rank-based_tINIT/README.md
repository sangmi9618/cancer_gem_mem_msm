# Evaluation of model extraction methods (MEMs) and model simulation methods (MSMs) #

### How to install
---
This code works on Linux and has been tested on Ubuntu 16.04.

1. &nbsp;Clone the repository
```
git clone https://sangmi9618@bitbucket.org/sangmi9618/cancer_gem_mem_msm.git
```
2. &nbsp;Download required software programs and add the repository to the MATLAB path  
    **Required software**  
    - A functional MATLAB installation (MATLAB R2018 or newer)  
    - [The RAVEN Toolbox](https://github.com/SysBioChalmers/RAVEN)      
    - [The COBRA Toolbox](https://github.com/opencobra/cobratoolbox)      
    - [Human-GEM repository](https://github.com/SysBioChalmers/Human-GEM)
    - Optimization solver (Gurobi Optimizer 9.0.2 was used in this study)

---
This directory consists of two MATLAB files: **getINITModel2_rankBased.m** and **scoreRankBasedComplexModel.m** (rank-based tINIT related functions). These functions are adapted from the functions in the Human-GEM repository.


Rank-based tINIT
---
### Implementation
1. &nbsp;Prepare generic human GEM (e.g., Human-GEM.mat file in the Human-GEM repository), gene expression data (e.g., ../MSMs/sampleInputFile/expression/sampleExpression.csv), and metabolic task list (e.g.,metabolicTasks_Essential.xlsx file in the Human-GEM repository).  Please refer to the format of gene expression data in sampleInputFile directory.

2. &nbsp;Run rank-based tINIT to reconstruct patient-specific GEMs. The following inputs are required:  
    `model` # the generic human GEM from which the patient-specific GEM will be extracted  
    `rna_data_struct` # data structure with fields: tissues, genes, levels, and threshold; containing gene expression information 
    `sample` # sample name; must match the name in rna_data_struct.tissues   
    `essentialTasks` # metabolic task structure; use the RAVEN `parseTaskList` function to create this 

	```
    outputmodel = getINITModel2_rankBased(model, sample, [], [], rna_data_struct, [], true, [], true, true, essentialTasks, [],[])
	```

Run time for this source code is dependent on the values set to `TimeLimit` parameter in RAVEN `optimizeProb` function.

---
### Reference
To be added here.
