import glob
import argparse
import copy
import logging
import os
import time
import warnings

import pandas as pd
from cobra.io import read_sbml_model, load_matlab_model
from GemSimulation.LADsimulationFunctions import omics_score_calculation
from GemSimulation.LADsimulationFunctions.utils import *

def parse_getpra_information(getpra_file):
    getrpa_sl_info = {}
    fp = open(getpra_file, 'r')
    fp.readline()
    for line in fp:
        sptlist = line.strip().split('\t')
        ucsc_id = sptlist[4].strip()
        compartment = sptlist[8].strip()

        if ucsc_id not in getrpa_sl_info:
            getrpa_sl_info[ucsc_id] = [compartment]
        else:
            getrpa_sl_info[ucsc_id].append(compartment)

    fp.close()

    return getrpa_sl_info


def calculate_reaction_score(cobra_model, expression_score, getrpa_sl_info, use_getpra_filtering=False):
    expression_score_string = {}
    for gene_id in expression_score:
        expression_score_string[str(gene_id)] = expression_score[gene_id]

    reaction_weights = {}
    
    for each_reaction in cobra_model.reactions:
        temp_expression_score_string = copy.deepcopy(expression_score_string)

        compartments = [metabolite.compartment for metabolite in each_reaction.reactants + each_reaction.products]
        compartments = list(set(compartments))
        reaction_weights[each_reaction.id] = 0.0
        GPR_association = each_reaction.gene_reaction_rule
        genes = [gene.id for gene in each_reaction.genes]
        genes = list(set(genes) & set(temp_expression_score_string.keys()))

        if use_getpra_filtering:
            for each_gene in genes:
                if each_gene in getrpa_sl_info:
                    getpra_compartments = getrpa_sl_info[each_gene]
                    if len(set(getpra_compartments) & set(compartments)) == 0:
                        temp_expression_score_string[each_gene] = 0.0

        if len(genes) > 1:
            GPR_list = convert_string_GPR_to_list_GPR(GPR_association)
            expression = omics_score_calculation.GPR_score_calculation(GPR_list, temp_expression_score_string)
            reaction_weights[each_reaction.id] = expression
        elif len(genes) == 1:
            gene_id = genes[0].strip()
            if gene_id in temp_expression_score_string:
                expression = temp_expression_score_string[gene_id]
                reaction_weights[each_reaction.id] = expression

    return reaction_weights

def calculate_reaction_weight(output_dir, omics_file, getpra_file, use_getpra, cobra_model, generic_model): 
    getrpa_sl_info = parse_getpra_information(getpra_file)
       
    basename = os.path.basename(omics_file).split('.')[0].strip()
    expression_score = pd.read_csv(omics_file, index_col=0)
    metabolic_genes = [gene.id for gene in generic_model.genes]
    expression_score=expression_score.loc[set(metabolic_genes)&set(expression_score.index)]
    expression_score = expression_score.to_dict()['Expression']
    
    reaction_weight_info = {}
    if use_getpra == True:
        reaction_weights = calculate_reaction_score(cobra_model, expression_score, getrpa_sl_info, True)
    else:
        reaction_weights = calculate_reaction_score(cobra_model, expression_score, getrpa_sl_info, False)

    reaction_weight_info[basename] = reaction_weights

    df = pd.DataFrame.from_dict(reaction_weight_info)
    df.to_csv(output_dir + '/Reaction_weight_%s.csv' % (basename))
    return