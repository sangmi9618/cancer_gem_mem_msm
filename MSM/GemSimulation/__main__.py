from GemSimulation import inputParser
import glob, os
import cobra
import pandas as pd
import pkg_resources
import copy
import numpy as np

from GemSimulation.run_LADsimulation import runLADsimulation
from GemSimulation.LADsimulationFunctions.utils import *
from GemSimulation import simpleSimulation
from GemSimulation import E_flux2_spot_simulator

import multiprocessing
from multiprocessing import Pool, Manager


def loadModels(item):
    tempModel=cobra.io.load_matlab_model(item)
    return tempModel


def mediumModel(modelf, medium_info):
    result_model=copy.deepcopy(modelf)
    
    for rxn in modelf.reactions:
        if len(rxn.metabolites)==1:
            metabol=[x for x in rxn.metabolites][0].elements
            if rxn.id in medium_info:
                if 'C' in metabol:
                    edit=result_model.reactions.get_by_id(rxn.id)
                    edit.bounds=(-10,1000.0)
                else:
                    edit=result_model.reactions.get_by_id(rxn.id)
                    edit.bounds=(-1000,1000.0)
                
            else:
                edit=result_model.reactions.get_by_id(rxn.id)
                edit.bounds=(0,1000.0)
                
    return result_model


def multi_run(params, fba_dict, pfba_dict, rxtome_dict, spot_dict, eflux2_dict, modelPath):
    
    output_dir = params['output_dir']
    hamMedia = params['hamMedia']

    getpraFile = params['getpraFile']
    dis_lad_flag = params['dis_lad_flag']
    dis_fba_flag = params['dis_fba_flag']
    dis_spot_ef_flag = params['dis_spot_ef_flag']
    dis_rxn_flag = params['dis_rxn_flag']
    rel = params['rel']
    
    human1Model = './GemSimulation/data/Human-GEM.xml'
    human1Model = cobra.io.read_sbml_model(human1Model)
    
    for singModel in modelPath:
        DONOR=singModel.split('/')[-1].split('.')[0]
        
        COBRAMODELS = loadModels(singModel)
        COBRAMODELSM = mediumModel(COBRAMODELS, hamMedia)
        
        if not dis_lad_flag:
            runLADsimulation(output_dir+'splited_omics_data/'+DONOR+'.csv', COBRAMODELSM, human1Model, output_dir+'LAD', getpraFile) 
            
        if not dis_fba_flag:
            fbaS = simpleSimulation.runFBA(COBRAMODELSM)
            pFBAS = simpleSimulation.runpFBA(COBRAMODELSM)
            fba_dict[DONOR] = fbaS
            pfba_dict[DONOR] = pFBAS
            
        if not dis_spot_ef_flag:
            rw_path = output_dir+'LAD/Reaction_weight_'+DONOR+'.csv'
            rw = pd.read_csv(rw_path, index_col=0)
            rw = rw[DONOR].to_dict()

            simulator = E_flux2_spot_simulator.simulator(COBRAMODELSM, rw)
            eFlux2_sol = simulator.run_E_flux2(rel=rel)
            spot_sol = simulator.run_spot(hamMedia)
            
            if not type(spot_sol) == tuple:
                spot_dict[DONOR] = spot_sol
            else:
                
                SPOT_status = spot_sol[0]
                SPOT_alt_sol = spot_sol[1]
                pd.DataFrame({'alt':SPOT_alt_sol}).to_csv(output_dir+'fails/%s_SPOT.csv'%DONOR)
                
                f = open(output_dir+'fails/%s_SPOT_status.txt'%DONOR, 'w')
                f.write(str(SPOT_status))
                f.close()
                
            if not type(eFlux2_sol) == tuple:
                eflux2_dict[DONOR] = eFlux2_sol
            else:
                
                eFlux2_status = eFlux2_sol[0]
                eFlux2_alt_sol = eFlux2_sol[1]
                pd.DataFrame({'alt':eFlux2_alt_sol}).to_csv(output_dir+'fails/%s_eFlux2.csv'%DONOR)
                
                f = open(output_dir+'fails/%s_eFlux2_status.txt'%DONOR, 'w')
                f.write(str(eFlux2_status))
                f.close()
            
        if not dis_rxn_flag:
            rxns = simpleSimulation.getReactome(COBRAMODELSM)
            rxtome_dict[DONOR] = rxns
        
        del COBRAMODELS
        del COBRAMODELSM
        

def main():
    
    parser = inputParser.argument_parser()
    options = parser.parse_args()
    
    output_dir = options.output_dir
    model_dir = options.input_dir

    if not os.path.isdir(output_dir):
        os.mkdir(output_dir)
        
    if output_dir[-1]!='/':
        output_dir+='/'
    if model_dir[-1]!='/':
        model_dir+='/'
    
    exp_path = options.expression
    
    dis_lad_flag = options.disable_lad
    dis_fba_flag = options.disable_fba
    dis_spot_ef_flag = options.disable_spot_ef
    dis_rxn_flag = options.disable_reactome
    

    num_core = int(options.num_core)
    rel = float(options.eFlux2_rel)

    ## loadData
    hamMedia = './GemSimulation/data/hamMedium.csv'
    hamMedia = pd.read_csv(hamMedia)
    hamMedia = hamMedia['rxns'].values

    getpraFile = './GemSimulation/data/GeTPRA.txt'
	## split expression data by sample
    split_expression_data_by_column(exp_path, output_dir + 'splited_omics_data/')
    print("Done split expression data!")
    
    ## main
    base=glob.glob(model_dir+'GEMs/*')

    ladFails = []
    fbaFails = []
    
    pool = multiprocessing.Pool(num_core)
    m = Manager()
    
    FBADICT = m.dict()
    pFBADICT = m.dict()
    REACTOMEDICT = m.dict()
        
    SPOT_DICT = m.dict()
    Eflux2_DICT = m.dict()
    
    all_paths = np.array_split(base, num_core)
    
    params = {}
    params['output_dir'] = output_dir
    params['hamMedia'] = hamMedia
    params['getpraFile'] = getpraFile

    params['dis_lad_flag'] = dis_lad_flag
    params['dis_fba_flag'] = dis_fba_flag
    params['dis_spot_ef_flag'] = dis_spot_ef_flag
    params['dis_rxn_flag'] = dis_rxn_flag
    params['rel'] = rel
    
    '''MSMs failed to find optimal solution will be recorded here'''
    if not os.path.isdir(output_dir+'fails'):
        os.mkdir(output_dir+'fails')
    
    pool.starmap(multi_run, [(params, FBADICT, pFBADICT, REACTOMEDICT, SPOT_DICT, Eflux2_DICT,model_paths) for model_paths in all_paths])
    pool.close()
    pool.join()
    
    

    if not dis_fba_flag:
        FBADF=pd.DataFrame.from_dict(dict(FBADICT), orient='index')
        FBADF.T.to_csv(output_dir+'FBA.csv')
    
        pFBADF=pd.DataFrame.from_dict(dict(pFBADICT), orient='index')
        pFBADF.T.to_csv(output_dir+'pFBA.csv')
        
    if not dis_rxn_flag:
        REACTOMEDF=pd.DataFrame.from_dict(dict(REACTOMEDICT), orient='index')
        REACTOMEDF.T.to_csv(output_dir+'reactome.csv')
    
    if not dis_spot_ef_flag:
        SPOT_DF=pd.DataFrame.from_dict(dict(SPOT_DICT), orient='index')
        SPOT_DF.T.to_csv(output_dir+'spot.csv')
        
        Eflux2_DF=pd.DataFrame.from_dict(dict(Eflux2_DICT), orient='index')
        Eflux2_DF.T.to_csv(output_dir+'E_flux2.csv')
        
    ## organize lad solutions
    ladFluxes = glob.glob(output_dir+'LAD/*')
    LADs={}
    for i in [x for x in ladFluxes if x.split('LAD/')[-1][0]=='F']:
        pid=i.split('ion_')[-1].split('.')[0]

        df=pd.read_csv(i, index_col=0)
        LADs[pid]=df['Flux'].to_dict()
    pd.DataFrame(LADs).to_csv(output_dir+'LAD.csv')

