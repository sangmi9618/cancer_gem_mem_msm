import multiprocessing
from multiprocessing import Pool, Manager
import numpy as np
import time
import pandas as pd

def MultiRun(function, output_dir, model_base, weight_dir, num_cores, hamMedia):
    num_cores = num_cores
    start = time.time()
    
    pool = multiprocessing.Pool(num_cores)
    m = Manager()
    result = m.dict()
    failed = m.list()
    all_paths = np.array_split(model_base, num_cores)
    # use starmap to send multiple arguments
    pool.starmap(function, [(result, failed, output_dir, model_paths, weight_dir, hamMedia) for model_paths in all_paths])
    pool.close()
    pool.join()

    resultDF = pd.DataFrame.from_dict(dict(result), orient='index')
    failedDF = pd.DataFrame(list(failed), columns = ['SampleID'])
    
    return resultDF, failedDF