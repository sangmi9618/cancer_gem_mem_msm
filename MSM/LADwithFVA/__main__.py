from LADwithFVA import argParser
from LADwithFVA.MultiRun import MultiRun
from gurobipy import *
import cobra
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import copy
import time
import logging

class Simulator(object):
    def __init__(self, model, reaction_weight_file, scaling_factor=100.0):
        self.model = model
        
        self.rxns = [x.id for x in self.model.reactions]
        self.irr_rxn = [x.id for x in self.model.reactions if x.reversibility==False]
        self.rev_rxn = [x.id for x in self.model.reactions if x.reversibility==True]
        self.irr_rxn_back=[]
        
        self.S = self.parseS()
#         print(len(self.irr_rxn), len(self.rev_rxn))

        self.no_gpr = []
        for r in self.model.reactions:
            if len(r.genes)==0:
                self.no_gpr.append(r.id)

        weight = pd.read_csv(reaction_weight_file, index_col=0)
        self.sample_id = weight.columns[0]
        self.opt_flux = weight.loc[[x for x in self.rxns if not x in self.no_gpr]].div(scaling_factor)
        self.opt_flux = self.opt_flux.to_dict()[self.opt_flux.columns[0]]

        self.flux_constraints = {}
        self.flux_constraints['biomass_human'] = [0.01, 1000.0]
        
    def parseS(self):
        S={}
        for r in self.model.reactions:
            temp = {}
            for m in r.metabolites:
                if r.id in self.irr_rxn_back:
                    temp[m.id]=-r.get_coefficient(m.id)
                else:
                    temp[m.id]=r.get_coefficient(m.id)
            S[r.id]=temp
        S = pd.DataFrame(S)
        S = S.fillna(0)
        S = S[self.rxns]
        
        return S
    
    def LAD(self):
        m = Model('LAD')
        m.setParam('OutputFlag', 0)
        m.reset()
        
        V_r={}
        fplus = {}
        fminus = {}
        for rxn in self.rxns:
            if rxn in self.flux_constraints:
                V_r[rxn] = m.addVar(lb=self.flux_constraints[rxn][0], ub=self.flux_constraints[rxn][1], name=rxn)
            else:
                V_r[rxn] = m.addVar(lb=self.model.reactions.get_by_id(rxn).bounds[0], 
                                 ub=self.model.reactions.get_by_id(rxn).bounds[1], name=rxn)
            fplus[rxn] = m.addVar(lb=0.0, ub=1000.0, name=rxn)
            fminus[rxn] = m.addVar(lb=0.0, ub=1000.0, name=rxn)
                
        m.update()
        
        for rxn in self.opt_flux:
            m.addConstr(V_r[rxn] == (fplus[rxn] - fminus[rxn]))
            m.addConstr(fplus[rxn], GRB.GREATER_EQUAL, V_r[rxn] - self.opt_flux[rxn], name=rxn)
            m.addConstr(fminus[rxn], GRB.GREATER_EQUAL, self.opt_flux[rxn] - V_r[rxn], name=rxn)            
        m.update()
        
        S_matrix = self.S
        for met in S_matrix.index:
            S_sub = S_matrix.loc[met]
            S_idx = S_sub[S_sub!=0.0].index
            m.addConstr(quicksum([S_sub[call]*V_r[call] for call in S_idx])==0)
        m.update()
        
                
        self.target_reactions = [x for x in self.irr_rxn if x in self.opt_flux]
        m.setObjective(quicksum(((fplus[each_reaction] + fminus[each_reaction]) - self.opt_flux[each_reaction]) 
                                for each_reaction in self.target_reactions), GRB.MINIMIZE)
          
        
        m.optimize()
        
        if m.status != 3:
            ReactionFlux = {}
            for rxn in self.rxns:
                if rxn in self.irr_rxn_back:
                    ReactionFlux[rxn] = -float(V_r[rxn].x)
                else:
                    ReactionFlux[rxn] = float(V_r[rxn].x)
            return m.status, m.ObjVal, ReactionFlux
        else:
            return m.status, False, False

    
    def FVA(self, rxn_obj, mode='max'):
        fx = Model('FVA')
        fx.setParam('OutputFlag', 0)
        fx.reset()
        
        V_r=[]
        for rxn in self.rxns:
            V_r.append(fx.addVar(lb=self.model.reactions.get_by_id(rxn).bounds[0], 
                                 ub=self.model.reactions.get_by_id(rxn).bounds[1],  name=rxn))
                                                                              
        fx.update()
        
        S_matrix = self.parseS().values()
        for idx in range(S_matrix.shape[0]):
            S_sub = S_matrix[idx,:]
            S_idx = np.nonzero(S_sub)[0]
            fx.addConstr(quicksum([S_sub[call]*V_r[call] for call in S_idx])==0)
        fx.update()    
            
        idx = self.rxns.index(rxn_obj)
        V_obj = V_r[idx]
        if mode == 'max':
            fx.setObjective(V_obj, GRB.MAXIMIZE)
        elif mode == 'min':
            fx.setObjective(V_obj, GRB.MINIMIZE)
        fx.optimize()
        
        if fx.status == 2:
            sol = {x.VarName:x.x for x in V_r}
            return fx.status, fx.ObjVal, sol
        else:
            return fx.status, False, False
        
    def FVA_with_LADobjVal(self, LADobjVal, rxn_obj, mode='max'):
#         start = time.time()
#         logging.basicConfig(format='%(levelname)s: %(message)s', level=logging.INFO)
        m = Model('LAD_FVA')
        m.setParam('OutputFlag', 0)
        m.reset()
        
        V_r={}
        for rxn in self.rxns:
            if rxn in self.flux_constraints:
                V_r[rxn] = m.addVar(lb=self.flux_constraints[rxn][0], ub=self.flux_constraints[rxn][1], name=rxn)
            else:
                V_r[rxn] = m.addVar(lb=self.model.reactions.get_by_id(rxn).bounds[0], 
                                 ub=self.model.reactions.get_by_id(rxn).bounds[1], name=rxn)
                
        m.update()
        
        diff={}
        for rxn in self.target_reactions:
            diff[rxn] = m.addVar(vtype=GRB.CONTINUOUS, lb=-GRB.INFINITY, ub=GRB.INFINITY, name='diff_'+rxn)
            m.addConstr(diff[rxn] == (V_r[rxn] - self.opt_flux[rxn]))
        m.update()   
        
        abs_diff={}
        for rxn in self.target_reactions:
            abs_diff[rxn] = m.addVar(vtype=GRB.CONTINUOUS, lb=0, ub=GRB.INFINITY, name='absdiff_'+rxn)
            m.addConstr(abs_diff[rxn] == abs_(diff[rxn]))
            
        m.addConstr(quicksum(abs_diff[each_reaction] for each_reaction in self.target_reactions), GRB.LESS_EQUAL, LADobjVal)            
        m.update()
        
        S_matrix = self.S
        for met in S_matrix.index:
            S_sub = S_matrix.loc[met]
            S_idx = S_sub[S_sub!=0.0].index
            m.addConstr(quicksum([S_sub[call]*V_r[call] for call in S_idx])==0)
        m.update()        

        if mode == 'max':
            m.setObjective(V_r[rxn_obj], GRB.MAXIMIZE)
        elif mode == 'min':
            m.setObjective(V_r[rxn_obj], GRB.MINIMIZE)
        m.optimize()
#         logging.info(time.strftime("Elapsed time %H:%M:%S", time.gmtime(time.time() - start)))
        if m.status != 3:
            return m.status, m.ObjVal
        else:
            return m.status, False
        
    def LAD_FVA_cycle(self, LADobjVal):
        new_irr_forw = []
        new_irr_back = []
        # The bounds of newly defined irreversible reactions should not be changed the FVA iteration of other reversible reactions.    
        # So, first deepcopy orginal model and then change bounds. After all the iterations, then assign changed model as self.model.
        result_model = copy.deepcopy(self.model)
        for i, rxn in enumerate(self.rev_rxn):
            stat, minVal = self.FVA_with_LADobjVal(LADobjVal, rxn, 'min') 
            if stat==2 and minVal >=0: # irreversibly forward
                edit = result_model.reactions.get_by_id(rxn)
                lb, ub = edit.bounds
                edit.bounds = (max(lb, 0), ub)
                new_irr_forw.append(rxn)
                
            else:
                stat, maxVal = self.FVA_with_LADobjVal(LADobjVal, rxn, 'max')
                if stat==2 and maxVal <=0: # irreversibly backward
                    edit = result_model.reactions.get_by_id(rxn)
                    lb, ub = edit.bounds
                    ub = -ub
                    lb = -lb
                    edit.bounds = (max(ub, 0), lb)
                    new_irr_back.append(rxn)
                else:
                    pass
                
            if (i+1)%100==0:
                print(f'{i+1} reactions done! irr_forward: {len(new_irr_forw)}, irr_backward: {len(new_irr_back)}')
                
        self.model = result_model
        self.irr_rxn.extend(new_irr_forw)
        self.irr_rxn.extend(new_irr_back)
        self.irr_rxn_back.extend(new_irr_back)
        self.rev_rxn = [x for x in self.rev_rxn if not x in self.irr_rxn]
        self.S = self.parseS()
        return

def mediumModel(modelf, medium_info):
    result_model=copy.deepcopy(modelf)
    
    for rxn in modelf.reactions:
        if len(rxn.metabolites)==1:
            metabol=[x for x in rxn.metabolites][0].elements
            if rxn.id in medium_info:
                if 'C' in metabol:
                    edit=result_model.reactions.get_by_id(rxn.id)
                    edit.bounds=(-10,1000.0)
                else:
                    edit=result_model.reactions.get_by_id(rxn.id)
                    edit.bounds=(-1000,1000.0)
                
            else:
                edit=result_model.reactions.get_by_id(rxn.id)
                edit.bounds=(0,1000.0)
                
    return result_model

def simulationFunction_LAD_FVA(fluxDict, failed_samples, output_dir, model_base, weight_dir, hamMedia):
    for i in model_base:
        dn=i.split('/')[-1].split('.')[0]
        
        if os.path.exists(output_dir+f'Flux_{dn}.csv'):
            print("Already flux file exists!")
            continue      
            
        model = cobra.io.load_matlab_model(i)
        model = mediumModel(model, hamMedia)
        reaction_weight = weight_dir + f"Reaction_weight_{dn}.csv"
        print(dn, reaction_weight)
        
        start = time.time()
        formatter = logging.Formatter('%(levelname)s: %(message)s')
        logger = logging.getLogger(__name__)
        logger.setLevel(logging.INFO)
        log_dir = output_dir+'logs/'
        if not os.path.isdir(log_dir):
            os.mkdir(log_dir)
            
        handler = logging.FileHandler(log_dir + f'{dn}.log', mode='w')
        handler.setFormatter(formatter)
        logger.addHandler(handler)
        
        simul = Simulator(model, reaction_weight)
        logger.info(f"Num of irreversible reactions: {len(simul.irr_rxn)}; Num of reversible reactions: {len(simul.rev_rxn)}")
        prev_irr_rxn_nums = 0
        cycle=0
        while len(simul.irr_rxn) > prev_irr_rxn_nums:
            cycle+=1
            prev_irr_rxn_nums = len(simul.irr_rxn)
            status, LADobjVal, LADFlux = simul.LAD()
            logger.info(f"Cycle #{cycle} LAD info; status: {status}; obj val: {LADobjVal}") 
            if status != 3:
                simul.LAD_FVA_cycle(LADobjVal)
                logger.info(f"Done cycle #{cycle}; Remained num of reversible reactions: {len(simul.rev_rxn)}")
                logger.info(time.strftime("Elapsed time %H:%M:%S", time.gmtime(time.time() - start)))
            else:
                rxn_info = {"Rev rxns": simul.rev_rxn, "Irr rxns":simul.irr_rxn, "Irr back rxns":simul.irr_rxn_back}
                pd.DataFrame.from_dict(rxn_info, orient='index').T.to_csv(output_dir+f"Cycle{cycle}_{dn}_rxn_info.csv", index=False)
                
        if not LADFlux:
            failed_samples.append(dn)
            handler.close()
            logger.removeHandler(handler)
            continue
            
        else:    
            LADDF=pd.DataFrame.from_dict(LADFlux, orient='index', columns=['Flux'])
            LADDF.to_csv(output_dir+f'Flux_{dn}.csv')
            fluxDict[dn] = LADFlux       
            handler.close()
            logger.removeHandler(handler)
        
def run(output_dir, modelPaths, weight_dir, coreNum, hamMedia):
    fluxDf, failDf = MultiRun(simulationFunction_LAD_FVA, output_dir, modelPaths, weight_dir, coreNum, hamMedia)
    fluxDf.T.to_csv(output_dir+"LAD_FVA.csv")
    failDf.to_csv(output_dir+"failed_sample_list.csv")

def main():
    parser = argParser.argument_parser()
    options = parser.parse_args()
    
    coreNum=int(options.numOfJobs)   
    output_dir = options.output_dir
    model_dir = options.input_dir
    
    if not os.path.isdir(output_dir):
        os.mkdir(output_dir)
    
    if not output_dir[-1]=='/':
        output_dir=output_dir+'/'
        
    if not model_dir[-1]=='/':
        model_dir=model_dir+'/'

    hamMedia = pd.read_csv('./GemSimulation/data/hamMedium.csv')['rxns'].values
    model_base = glob.glob(model_dir+'GEMs/*')
#     model_base = glob.glob(model_dir+'GEMs/DO45225*')
    weight_dir = options.reaction_weight_dir
    
    run(output_dir, model_base, weight_dir, coreNum, hamMedia)
