# Evaluation of model extraction methods (MEMs) and model simulation methods (MSMs) #

### How to install
---
This code works on Linux and has been tested on Ubuntu 16.04.

1. Clone the repository
```
git clone https://sangmi9618@bitbucket.org/sangmi9618/cancer_gem_mem_msm.git
```
2. Create and activate a conda environment
```
conda env create -f simulation_env.yaml
(conda env create -f ./MSM/simulation_env.yaml)
source activate benchMark
```
---
This directory consists of two python files: model simulation methods (**runSimulation.py**) and iterariveLAD (**runLADwithFVA.py**).


1.&nbsp;Model simulation methods
---
### Implementation
1. &nbsp;Prepare patient-specific GEMs (e.g., sampleInputFile/GEMs for a toy input) and expression data (e.g., sampleInputFile/expression/sampleExpression.csv for a toy input). Please refer to the format of each file in sampleInputFile directory.

2. &nbsp;Run MSM. 
    Following arguments are required:  
    -o/--output_dir, # output directory  
    -dir/--input_dir, # directory containing patient-specific GEMs  
    -exp/--expression # expression file  
        
    Following arguments are optional:  
    -ladD, --disable_lad # default False. If it is True, disable LAD simulation  
    -fbaD, --disable_fba # default False. If it is True, disable FBA and pFBA simulation  
    -spot_eflux2D, --disable_spot_ef # default False. If it is True, disable SPOT and E-Flux2 simulation  
    -rxnD, --disable_reactome # default False. If it is True, disable parsing reactome (i.e. reaction contents in patient-specific GEMs)  
    -n, --num_core # default 10. The number of CPU cores will be used to run MSM  
    -rel, --eFlux2_rel # default 0.99999. relaxation factor for E-Flux2. see the publication manuscript  
	
	#### Example
	```
	python runSimulation.py -o ./output/ -dir ./sampleInputFile/ -exp ./sampleInputFile/expression/sampleExpression.csv -n 2
	```
    
Output files such as "FBA.csv" will be generated in output directory after computation.  
Run time for the toy example given in this code is approximately 180 seconds.

---
2.&nbsp;IterativeLAD
---
### Implementation
1. &nbsp;Prepare patient-specific GEMs (e.g., sampleInputFile/GEMs for a toy input) and reaction weight files (available via runSimulation.py). Please refer to the format of each file in sampleInputFile directory.

2. &nbsp;Run iterativeLAD. The following arguments are required: 
    -o, --output_dir, # output directory  
    -dir, --input_dir, # directory containing patient-specific GEMs  
    --rw, --reaction_weight_dir # directory containing reaction weight files
        
    following arguments are optional arguments  
    -n, --numOfJobs # default 10. The number of CPU cores will be used  
	
	#### Example
	```
	python runLADwithFVA.py -o ./output/iterativeLAD -dir ./sampleInputFile/ -rw ./output/LAD/ -n 2
	```

Output files such as "Flux_"sampleID".csv" will be generated in output directory after computation.  
Run time for the toy example given in this code is approximately 15 hours.  

---
### Reference
To be added here.
